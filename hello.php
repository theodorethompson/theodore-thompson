<?php
// PHP code goes here

echo '
<!DOCTYPE html>
<html lang="en-US">

<html>
    <head>
        <title>Hello World</title>
    </head>
    <body>
        <h2>What do you say?</h2>
        <p>"Hello World"</p>
    </body>
</html>
';

?>

<?php

$obj = (object) array('foo' => 'bar', 'property' => 'value');

echo $obj->foo; // prints 'bar'
echo $obj->property; // prints 'value'
print " \n \t this is  {$obj->foo}!";

class Message { 
    public $type = 'message'; 
    public $time = 'time and date';
    public $id = 'Some id given by the system';
    private $owner = 'Pub key'; //owners public key
    private $recipient = 'pub key'; //to whom is the message ment for
    private $value = 'Hello word - with priv key'; //only open with private key
    
    function get($from,$to,$saywhat) {
        
        if(empty ($to)){
            return '100'; //err 100 now call no from
        } else {
            $this->time = '2015/10/21 21:34'; //Now
            $this->id = "database incr";
            $this->value = $saywhat;
            $this->recipient = $to;
        }
        
        return 0;
    }
    
    function testview() {
        print "Object name:  \n
            Type of object: $this->type
            Time of creation: $this->time
            Object ID: $this->id
            the owner key: $this->owner
            the recipient key: $this->recipient
            the content: $this->value";
	}
    
    function aMemberFunc() { 
        print 'Inside `aMemberFunc()`'; 
    } 
}
	
$test = new Message;
$test->get("a","b","I love to eat cake");
$test->testview();


$message = (object) array('name' => 'message', 'value' => 'hello world - private key', 'key' => 'public');


$user = (object) array('name' => 'user', 'property' => 'value');


$obj3 = (object) array('name' => 'obj3', 'property' => 'value');


$view = (object) array('name' => 'view', 'property' => 'value');


$obj5 = (object) array('foo' => 'bar', 'property' => 'value');


$obj6 = (object) array('foo' => 'bar', 'property' => 'value');



?>
